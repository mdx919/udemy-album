//import libraries
import React from 'react';
import { View, Text, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';


const AlbumDetail = ({album}) => {
  const { title, artist, thumbnail_image, image, url } = album;

  return (
      <Card>
        <CardSection>
          <View style={styles.thumbnailContainerStyle}>
            <Image
              style={styles.thumbnailStyle}
              source={{ uri: thumbnail_image }}
            />
          </View>
          <View style={styles.headerContentStyle}>
            <Text style={styles.headerText}>{title}</Text>
            <Text style={{ marginTop: -10 }}>{artist}</Text>
          </View>
        </CardSection>

        <CardSection>
            <Image
              style={styles.imageStyle}
              source={{ uri: image }}
            />
        </CardSection>

        <CardSection>
            <Button onPress={() => Linking.openURL(url)}>
                Buy now!
            </Button>
        </CardSection>
      </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },

  thumbnailStyle: {
    height: 80,
    width: 80,
  },

  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },

  headerText: {
    fontSize: 24,
  },

  imageStyle: {
    flex: 1,
    height: 300,
    width: null,
  },
};

export default AlbumDetail;
