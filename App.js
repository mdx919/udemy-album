//import libraries
import React from 'react';
import { View } from 'react-native';
import Header from './src/myComponents/header';
import AlbumList from './src/myComponents/AlbumList';

//create components
const App = () => {
  return (
    <View style={{flex:1}}>
    <Header headerText={'albums'} />
    <AlbumList />
    </View>
  );
};



//export the components
export default App;
